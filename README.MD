#Metode i tehnike testiranja programske pordške
Projekt iz kolegija Metode i Tehnike Testiranja Programske Podrške

Web stranica koju sam odlučio testirati je Uzi Shop(https://www.uzishop.hr). To je online webshop koji nudi mogućnost registracije koristnika, pretraživanja proizvoda, dodavanja proizvoda u košaricu itd.

Tijekom testiranja sam koristio više načina odabira elementa na stranici. Koristio sam XPath, ID i Name u ovisnosti o tome što je bilo dostupno na pojedinom elementu. Koristio sam Thread.Sleep naredbu kako bi napravio razmak između naredbi zbog toga kako bi se stranica stigla učitati na pregledniku.
Koristio sam priority u testovima kako bi odredio redosljed kojim će se obavljati testovi.
##Klase

###Setup
Klasa koja služi za inicijalizaciju drivera i postavljanje URL-a stranice za testiranje

###BrowserSetup
Klasa koja služi za odabir preglednika kojeg želimo koristiti prilikom testiranja. Imamo mogućnost odabira GoogleChroma, Opere i MicrosoftEdga

###CartTest
Klasa unutar koje sam testirao funkcinalnost košarice. Dodavanje i uklanjanje proizvoda te povećavanje broja proizvoda u košarici.

###LoginTest
Klasa unutar koje sam testirao funkcionalnost dijela za prijavu.

###SearchTest
Klasa u kojoj sam obavljao testove vezane uz funkcionalnost pretrage.

##Testovi
###CartTest
1. AddToCart - u ovom testu provjeravam je li proizvod koji smo dodali u košaricu zaista u košarici.
2. updateCart - test koji se nastavlja na prethodni te u ovom testu mjenjam količinu proizvoda i provjeravam je li se košarica ažurirala
3. emptyCart - test u kojem brišem sve iz košarice i provjeravam je li košarica prazna

###LoginTest
1. SendEmptyRequest - test u kojem se šalje prazna login forma i provjerava se povratna poruka
2. SendInvalidEmailForm - test u kojem se šalje nepravilan oblik email adrese
3. SendOnlyEmail - test u kojem se šalje samo email adresa bez šifre
4. SendUnregisteredUser -  test u kojem se šalje zahtijev za prijavu od nepostojećeg korisnika

###SearchTest
1. EmptySearch - provjerava se respons tražilice nakon praznog zahtjeva
2. UnknownStringSearch - provjerava se pretraga nepostojećeg stringa
3. KnownStringSearch - provjerava se rezultat pretrage s postojećim stringom



#Ante Ukić