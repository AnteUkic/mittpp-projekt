import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;



public class LoginTest extends Setup{
    @BeforeSuite
    public void setup()  {
        driver.get(testURL);
        driver.manage().window().maximize();
    }

    @Test(priority = 1)
    public void SendEmptyRequest() throws InterruptedException {
        Thread.sleep(1500);
        WebElement login = driver.findElement(new By.ByXPath("/html/body/div[2]/div[1]/header/div[2]/div/div/nav/div[1]/div[1]/a"));
        login.click();
        Thread.sleep(1500);
        WebElement submitLogin = driver.findElement(new By.ById("SubmitLogin"));
        submitLogin.click();
        Thread.sleep(1500);
        WebElement errorMessage = driver.findElement(new By.ByXPath("/html/body/div[2]/div[2]/div/div/div[3]/div[2]/div[1]/ol/li"));
        Assert.assertEquals(errorMessage.getText(),"E-mail adresa je obvezna");
    }
    @Test(priority = 2)
    public void SendInvalidEmailForm() throws InterruptedException {
        Thread.sleep(1500);
        WebElement emailTextBox = driver.findElement(new By.ById("email"));
        emailTextBox.clear();
        emailTextBox.sendKeys("anteukic");
        Thread.sleep(1500);
        WebElement submitLogin = driver.findElement(new By.ById("SubmitLogin"));
        submitLogin.click();
        Thread.sleep(1500);
        WebElement errorMessage = driver.findElement(new By.ByXPath("/html/body/div[2]/div[2]/div/div/div[3]/div[2]/div[1]/ol/li"));
        Assert.assertEquals(errorMessage.getText(),"Neispravan e-mail adresa");
    }
    @Test(priority = 3)
    public void SendOnlyEmail() throws InterruptedException {
        Thread.sleep(1500);
        WebElement emailTextBox = driver.findElement(new By.ById("email"));
        emailTextBox.clear();
        emailTextBox.sendKeys("anteukic@hotmail.com");
        Thread.sleep(1500);
        WebElement submitLogin = driver.findElement(new By.ById("SubmitLogin"));
        submitLogin.click();
        Thread.sleep(1500);
        WebElement errorMessage = driver.findElement(new By.ByXPath("/html/body/div[2]/div[2]/div/div/div[3]/div[2]/div[1]/ol/li"));
        Assert.assertEquals(errorMessage.getText(),"Lozinka je obvezna");
    }
    @Test(priority = 4)
    public void SendUnregisteredUser() throws InterruptedException {
        Thread.sleep(1500);
        WebElement emailTextBox = driver.findElement(new By.ById("email"));
        emailTextBox.clear();
        emailTextBox.sendKeys("anteukic@hotmail.com");
        Thread.sleep(1500);
        WebElement passwordTextBox = driver.findElement(new By.ById("passwd"));
        passwordTextBox.clear();
        passwordTextBox.sendKeys("1234567");
        Thread.sleep(1500);
        WebElement submitLogin = driver.findElement(new By.ById("SubmitLogin"));
        submitLogin.click();
        Thread.sleep(1500);
        WebElement errorMessage = driver.findElement(new By.ByXPath("/html/body/div[2]/div[2]/div/div/div[3]/div[2]/div[1]/ol/li"));
        Assert.assertEquals(errorMessage.getText(),"Provjera autentičnosti nije uspjela");
    }
    @AfterSuite
    public void shutDown(){
        driver.quit();
    }
}
