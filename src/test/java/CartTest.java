
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.*;


public class CartTest extends Setup{
    double singlePrice;
    @BeforeSuite
    public void setup()  {
        driver.get(testURL);
        driver.manage().window().maximize();
    }

    @Test(priority = 1)
    public void AddToCart() throws InterruptedException {
        Thread.sleep(2000);
        WebElement periferija = driver.findElement(new By.ByXPath("/html/body/div[2]/div[1]/header/div[3]/div/div/div[6]/ul/li[3]/a"));
        periferija.click();
        Thread.sleep(1500);
        WebElement mouse = driver.findElement(new By.ByXPath("/html/body/div[2]/div[2]/div/div/div[3]/div[1]/div[1]/div/ul/li[4]/ul/li[2]/a"));
        mouse.click();
        Thread.sleep(1500);
        WebElement product = driver.findElement(new By.ByXPath("/html/body/div[2]/div[2]/div/div/div[3]/div[2]/ul/li[1]/div[1]/div[1]/div/a[1]"));
        product.click();
        Thread.sleep(1500);
        WebElement addToCart = driver.findElement(new By.ByXPath("/html/body/div[2]/div[2]/div/div/div[3]/div/div/div[1]/div[3]/form/div/div[3]/div[1]/p/button"));
        addToCart.click();
        Thread.sleep(1500);
        WebElement back = driver.findElement(new By.ByXPath("/html/body/div[2]/div[1]/header/div[3]/div/div/div[4]/div[1]/div[2]/div[5]/span/span"));
        back.click();
        Thread.sleep(1500);
        WebElement cart = driver.findElement(new By.ByXPath("/html/body/div[2]/div[1]/header/div[3]/div/div/div[3]/div/a"));
        cart.click();
        Thread.sleep(1500);
        WebElement singlePrice = driver.findElement((new By.ById("total_product_price_95866_0_0")));
        String price1 = singlePrice.getText().replace(" kn","");
        String price = price1.replaceAll(",",".");
        this.singlePrice = Double.parseDouble(price);
        WebElement productName = driver.findElement(new By.ByXPath("/html/body/div[2]/div[2]/div/div/div[3]/div/div[4]/table/tbody/tr/td[2]/p/a"));
        Assert.assertEquals(productName.getText(),"Logitech M705 Wireless, Unifying");
        Thread.sleep(1000);
    }
    @Test(priority = 2)
    public void updateCart() throws InterruptedException {
        WebElement productCount = driver.findElement(new By.ByXPath("/html/body/div[2]/div[2]/div/div/div[3]/div/div[4]/table/tbody/tr/td[5]/input[2]"));
        productCount.clear();
        productCount.sendKeys("2");
        Thread.sleep(1500);
        WebElement fullPrice = driver.findElement(new By.ById("total_product_price_95866_0_0"));
        String price1 = fullPrice.getText().replace(" kn","");
        String price = price1.replaceAll(",",".");
        String realPrice = price.replaceAll(" ", "");
        Double assertPrice = Double.parseDouble(realPrice);
        Assert.assertEquals(singlePrice*2,assertPrice);
    }
    @Test(priority = 3)
    public void emptyCart() throws InterruptedException {
        Thread.sleep(1500);
        WebElement removeAll = driver.findElement(new By.ById("95866_0_0_0"));
        removeAll.click();
        Thread.sleep(1500);
        WebElement message = driver.findElement(new By.ByXPath("/html/body/div[2]/div[2]/div/div/div[3]/div/p"));
        Assert.assertEquals(message.getText(),"Vaša košarica je prazna.");
    }
    @AfterSuite
    public void shutDown(){
        driver.quit();
    }
}
