import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.opera.OperaDriver;

public class BrowserSetup {
    public static WebDriver browserSelection(String name){
        switch (name){
            case "Opera":
                System.setProperty("webdriver.chrome.driver", "operadriver.exe");
                return new OperaDriver();
            case "GoogleChrome":
                System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
                return new ChromeDriver();
            case "MicrosoftEdge":
                System.setProperty("webdriver.edge.driver", "msedgedriver.exe");
                return new EdgeDriver();
        }
        return new ChromeDriver();
    }
}
