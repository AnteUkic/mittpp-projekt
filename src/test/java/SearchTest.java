import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class SearchTest extends Setup{
    @BeforeSuite
    public void setup()  {
        driver.get(testURL);
        driver.manage().window().maximize();
    }
    @Test(priority = 1)
    public void EmptySearch() throws InterruptedException {
        Thread.sleep(1500);
        WebElement submitSearch = driver.findElement(new By.ByXPath("/html/body/div[2]/div[1]/header/div[3]/div/div/div[2]/form/button"));
        submitSearch.click();
        Thread.sleep(1500);
        WebElement resultMessage = driver.findElement(new By.ByXPath("/html/body/div[2]/div[2]/div/div/div[3]/div[2]/p"));
        Assert.assertEquals(resultMessage.getText(),"Molimo unesite pojam za pretragu");
    }
    @Test(priority = 2)
    public void UnknownStringSearch() throws InterruptedException {
        Thread.sleep(1500);
        WebElement textBox = driver.findElement(new By.ByXPath("/html/body/div[2]/div[1]/header/div[3]/div/div/div[2]/form/input[4]"));
        textBox.sendKeys("122345");
        Thread.sleep(1500);
        WebElement submitSearch = driver.findElement(new By.ByXPath("/html/body/div[2]/div[1]/header/div[3]/div/div/div[2]/form/button"));
        submitSearch.click();
        Thread.sleep(1500);
        WebElement resultMessage = driver.findElement(new By.ByXPath("/html/body/div[2]/div[2]/div/div/div[3]/div[2]/p"));
        Assert.assertEquals(resultMessage.getText(),"Nije pronađen rezultat za Vašu pretragu \"122345\".");
    }
    @Test(priority = 3)
    public void KnownStringSearch() throws InterruptedException {
        Thread.sleep(1500);
        WebElement textBox = driver.findElement(new By.ByXPath("/html/body/div[2]/div[1]/header/div[3]/div/div/div[2]/form/input[4]"));
        textBox.clear();
        textBox.sendKeys("Nvidia");
        Thread.sleep(1500);
        WebElement submitSearch = driver.findElement(new By.ByXPath("/html/body/div[2]/div[1]/header/div[3]/div/div/div[2]/form/button"));
        submitSearch.click();
        Thread.sleep(1500);
        WebElement resultMessage = driver.findElement(new By.ByXPath("/html/body/div[2]/div[2]/div/div/div[3]/div[2]/h1/span[1]"));
        Assert.assertEquals(resultMessage.getText(),"\"NVIDIA\"");
    }
    @AfterSuite
    public void shutDown(){
        driver.quit();
    }
}
